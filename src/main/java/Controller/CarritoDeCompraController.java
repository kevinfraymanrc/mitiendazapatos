/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controller;

import Modelo.Dao.CarritoDeComprasDAO;
import Modelo.Entity.CarritoDeComprasDTO;
import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author LATITUDE
 */
@WebServlet("/carritodecompra")
public class CarritoDeCompraController extends HttpServlet {

    CarritoDeComprasDAO carDAO = new CarritoDeComprasDAO();

    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String accion = req.getParameter("accion");
        if (accion != null) {
            switch (accion) {
                case "borrar":
                    this.borrar(req, resp);
                    break;
                case "editar":
                    this.editar(req, resp);
                    break;
                default:
                    listarCarritodecompra(req, resp);
            }
        } else {
            listarCarritodecompra(req, resp);
        }
    }

    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String accion = req.getParameter("accion");
        if (accion != null) {
            switch (accion) {
                case "crear":
                    this.crear(req, resp);
                    break;
                case "modificar":
                    this.modificar(req, resp);
                    break;
                default:
                    this.listarCarritodecompra(req, resp);
            }
        } else {
            this.listarCarritodecompra(req, resp);
        }
    }

    private void listarCarritodecompra(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        List<CarritoDeComprasDTO> carritodecompras = carDAO.consultar();
        req.setAttribute("carritodecompras", carritodecompras);
        req.getRequestDispatcher("carritodecompra/verCarritodecompras.jsp").forward(req, resp);
    }

    private CarritoDeComprasDTO consultarCli(int articulosSeleccionados) {
        for (CarritoDeComprasDTO c : carDAO.consultar()) {
            if (c.getArticulosSeleccionados() == articulosSeleccionados) {
                return c;
            }
        }
        return null;
    }

    private void listarCarritodecomprasVista(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

        CarritoDeComprasDAO carDAO = new CarritoDeComprasDAO();
        List<CarritoDeComprasDTO> carritodecompras = carDAO.consultar();
        req.setAttribute("carritodecompras", carritodecompras);
        req.getRequestDispatcher("carritodecompra/verCarritodecompras.jsp").forward(req, resp);
    }

    private void crear(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

        int articulosSeleccionados = Integer.parseInt(req.getParameter("articulosSeleccionados"));
        int valorUnitario = Integer.parseInt(req.getParameter("valorUnitario"));
        int valorTotal = Integer.parseInt(req.getParameter("valorTotal"));
        int realizarCompra = Integer.parseInt(req.getParameter("realizarCompra"));

        int registros = new CarritoDeComprasDAO().insertar(new CarritoDeComprasDTO(articulosSeleccionados, valorUnitario, valorTotal, realizarCompra));
        this.listarCarritodecompra(req, resp);
    }

    private void borrar(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

        int articulosSeleccionados = Integer.parseInt(req.getParameter("articulosSeleccionados"));
        int registro = new CarritoDeComprasDAO().borrar(articulosSeleccionados);

        this.listarCarritodecompra(req, resp);
    }

    private void editar(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

        int articulosSeleccionados = Integer.parseInt(req.getParameter("articulosSeleccionados"));

        CarritoDeComprasDTO carritodecompra = this.consultarCli(articulosSeleccionados);
        req.setAttribute("carritosdecompras", carritodecompra);
        req.getRequestDispatcher("carritodecompra/editarCarritodecompra.jsp").forward(req, resp);
    }

    public void modificar(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        int articulosSeleccionados = Integer.parseInt(req.getParameter("articulosSeleccionados"));
        int valorUnitario = Integer.parseInt(req.getParameter("valorUnitario"));
        int valorTotal = Integer.parseInt(req.getParameter("valorTotal"));
        int realizarCompra = Integer.parseInt(req.getParameter("realizarCompra"));

        int registro = new CarritoDeComprasDAO().actualizar(new CarritoDeComprasDTO(articulosSeleccionados, valorUnitario, valorTotal, realizarCompra));
        this.listarCarritodecompra(req, resp);
    }

}

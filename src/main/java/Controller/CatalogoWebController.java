/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controller;

import Modelo.Dao.CatalogoWebDAO;
import Modelo.Entity.CatalogoWebDTO;
import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author LATITUDE
 */
@WebServlet("/catalogoweb")
public class CatalogoWebController extends HttpServlet {

    CatalogoWebDAO catDAO = new CatalogoWebDAO();

    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String accion = req.getParameter("accion");
        if (accion != null) {
            switch (accion) {
                case "borrar":
                    this.borrar(req, resp);
                    break;
                case "editar":
                    this.editar(req, resp);
                    break;
                default:
                    listarCatalogoweb(req, resp);
            }
        } else {
            listarCatalogoweb(req, resp);
        }
    }

    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String accion = req.getParameter("accion");
        if (accion != null) {
            switch (accion) {
                case "crear":
                    this.crear(req, resp);
                    break;
                case "modificar":
                    this.modificar(req, resp);
                    break;
                default:
                    this.listarCatalogoweb(req, resp);
            }
        } else {
            this.listarCatalogoweb(req, resp);
        }
    }

    private void listarCatalogoweb(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        List<CatalogoWebDTO> catalogosweb = catDAO.consultar();
        req.setAttribute("catalogosweb", catalogosweb);
        req.getRequestDispatcher("catalogoweb/verCatalogosweb.jsp").forward(req, resp);
    }

    private CatalogoWebDTO consultarCli(String referencia) {
        for (CatalogoWebDTO w : catDAO.consultar()) {
            if (w.getReferencia() == referencia) {
                return w;
            }
        }
        return null;
    }

    private void listarCatalogoswebVista(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

        CatalogoWebDAO catDAO = new CatalogoWebDAO();
        List<CatalogoWebDTO> catalogosweb = catDAO.consultar();
        req.setAttribute("catalogosweb", catalogosweb);
        req.getRequestDispatcher("catalogoweb/verCatalogosweb.jsp").forward(req, resp);
    }

    private void crear(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

        String referencia = req.getParameter("referencia");
        String color = req.getParameter("color");
        int talla = Integer.parseInt(req.getParameter("talla"));
        String material = req.getParameter("material");

        int registros = new CatalogoWebDAO().insertar(new CatalogoWebDTO(referencia, color, talla, material));
        this.listarCatalogoweb(req, resp);
    }

    private void borrar(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

        String referencia = req.getParameter("referencia");

        int registro = new CatalogoWebDAO().borrar(referencia);
        this.listarCatalogoweb(req, resp);
    }

    private void editar(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

        String referencia = req.getParameter("referencia");

        CatalogoWebDTO catalogoweb = this.consultarCli(referencia);
        req.setAttribute("catalogosweb", catalogoweb);
        req.getRequestDispatcher("catalogoweb/editarCatalogosweb.jsp").forward(req, resp);
    }

    public void modificar(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

        String referencia = req.getParameter("referencia");
        String color = req.getParameter("color");
        int talla = Integer.parseInt(req.getParameter("talla"));
        String material = req.getParameter("material");

        int registro = new CatalogoWebDAO().actualizar(new CatalogoWebDTO(referencia, color, talla, material));
        this.listarCatalogoweb(req, resp);
    }

}

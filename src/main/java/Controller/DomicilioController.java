/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controller;

import Modelo.Dao.DomicilioDAO;
import Modelo.Entity.DomicilioDTO;
import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author LATITUDE
 */
@WebServlet("/domicilio")
public class DomicilioController extends HttpServlet {

    DomicilioDAO domDAO = new DomicilioDAO();

    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

        String accion = req.getParameter("accion");
        if (accion != null) {
            switch (accion) {
                case "borrar":
                    this.borrar(req, resp);
                    break;
                case "editar":
                    this.editar(req, resp);
                    break;
                default:
                    listarDomicilio(req, resp);
            }
        } else {
            listarDomicilio(req, resp);
        }
    }

    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

        String accion = req.getParameter("accion");
        if (accion != null) {
            switch (accion) {
                case "crear":
                    this.crear(req, resp);
                    break;
                case "modificar":
                    this.modificar(req, resp);
                    break;
                default:
                    this.listarDomicilio(req, resp);
            }
        } else {
            this.listarDomicilio(req, resp);
        }
    }

    private void listarDomicilio(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

        List<DomicilioDTO> domicilios = domDAO.consultar();
        req.setAttribute("domicilios", domicilios);
        req.getRequestDispatcher("domicilio/verDomicilios.jsp").forward(req, resp);
    }

    private DomicilioDTO consultarCli(int direccion) {

        for (DomicilioDTO d : domDAO.consultar()) {
            if (d.getDireccion() == direccion) {
                return d;
            }
        }
        return null;
    }

    private void listarClientesVista(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

        DomicilioDAO domDAO = new DomicilioDAO();
        List<DomicilioDTO> domicilios = domDAO.consultar();
        req.setAttribute("domicilios", domicilios);
        req.getRequestDispatcher("domcilio/verDomicilios.jsp").forward(req, resp);
    }

    private void crear(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

        int departamento = Integer.parseInt(req.getParameter("departamento"));
        int ciudad = Integer.parseInt(req.getParameter("ciudad"));
        int direccion = Integer.parseInt(req.getParameter("direccion"));

        int registros = new DomicilioDAO().insertar(new DomicilioDTO(departamento, ciudad, direccion));
        this.listarDomicilio(req, resp);
    }

    private void borrar(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

        int direccion = Integer.parseInt(req.getParameter("direccion"));

        int registro = new DomicilioDAO().borrar(direccion);
        this.listarDomicilio(req, resp);
    }

    private void editar(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

        int direccion = Integer.parseInt(req.getParameter("direccion"));

        DomicilioDTO domicilio = this.consultarCli(direccion);
        req.setAttribute("domicilios", domicilio);
        req.getRequestDispatcher("domicilio/editarDomicilio.jsp").forward(req, resp);
    }

    public void modificar(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

        int departamento = Integer.parseInt(req.getParameter("departamento"));
        int ciudad = Integer.parseInt(req.getParameter("ciudad"));
        int direccion = Integer.parseInt(req.getParameter("direccion"));

        int registro = new DomicilioDAO().actualizar(new DomicilioDTO(departamento, ciudad, direccion));
        this.listarDomicilio(req, resp);
    }

}

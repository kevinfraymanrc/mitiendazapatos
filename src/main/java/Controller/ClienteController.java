/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controller;

import Modelo.Dao.ClienteDAO;
import Modelo.Entity.ClienteDTO;
import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author LATITUDE
 */
@WebServlet("/cliente")
public class ClienteController extends HttpServlet {

    ClienteDAO cliDAO = new ClienteDAO();

    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String accion = req.getParameter("accion");
        if (accion != null) {
            switch (accion) {
                case "borrar":
                    this.borrar(req, resp);
                    break;
                case "editar":
                    this.editar(req, resp);
                    break;
                default:
                    listarCliente(req, resp);
            }
        } else {
            listarCliente(req, resp);
        }
    }

    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String accion = req.getParameter("accion");
        if (accion != null) {
            switch (accion) {
                case "crear":
                    this.crear(req, resp);
                    break;
                case "modificar":
                    this.modificar(req, resp);
                    break;
                default:
                    this.listarCliente(req, resp);
            }
        } else {
            this.listarCliente(req, resp);
        }
    }

    private void listarCliente(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<ClienteDTO> clientes = cliDAO.consultar();
        req.setAttribute("clientes", clientes);
        req.getRequestDispatcher("cliente/verClientes.jsp").forward(req, resp);
    }

    private ClienteDTO consultarCli(int cedula) {
        for (ClienteDTO c : cliDAO.consultar()) {
            if (c.getCedula() == cedula) {
                return c;
            }
        }
        return null;
    }

    private void listarClientesVista(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        ClienteDAO cliDAO = new ClienteDAO();
        List<ClienteDTO> clientes = cliDAO.consultar();
        req.setAttribute("clientes", clientes);
        req.getRequestDispatcher("cliente/verClientes.jsp").forward(req, resp);
    }

    private void crear(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        int cedula = Integer.parseInt(req.getParameter("cedula"));
        String nombre = req.getParameter("nombre");
        String apellido = req.getParameter("apellido");
        int telefono = Integer.parseInt(req.getParameter("telefono"));
        String login = req.getParameter("login");
        String contrasena = req.getParameter("contrasena");

        int registros = new ClienteDAO().insertar(new ClienteDTO(cedula, nombre, apellido, telefono, login, contrasena));
        this.listarCliente(req, resp);
    }

    private void borrar(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        int cedula = Integer.parseInt(req.getParameter("cedula"));
        int registro = new ClienteDAO().borrar(cedula);
        this.listarCliente(req, resp);
    }

    private void editar(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

        int cedula = Integer.parseInt(req.getParameter("cedula"));

        ClienteDTO cliente = this.consultarCli(cedula);
        req.setAttribute("clientes", cliente);
        req.getRequestDispatcher("cliente/editarCliente.jsp").forward(req, resp);
    }

    public void modificar(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        int cedula = Integer.parseInt(req.getParameter("cedula"));
        String nombre = req.getParameter("nombre");
        String apellido = req.getParameter("apellido");
        int telefono = Integer.parseInt(req.getParameter("telefono"));
        String login = req.getParameter("login");
        String contrasena = req.getParameter("contrasena");

        int registro = new ClienteDAO().actualizar(new ClienteDTO(cedula, nombre, apellido, telefono, login, contrasena));
        this.listarCliente(req, resp);
    }

}

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controller;

import Modelo.Dao.MetodosDePagoDAO;
import Modelo.Entity.MetodosDePagoDTO;
import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author LATITUDE
 */
@WebServlet("/metododepago")
public class MetodoDePagoController extends HttpServlet {

    MetodosDePagoDAO metDAO = new MetodosDePagoDAO();

    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String accion = req.getParameter("accion");
        if (accion != null) {
            switch (accion) {
                case "borrar":
                    this.borrar(req, resp);
                    break;
                case "editar":
                    this.editar(req, resp);
                    break;
                default:
                    listarMetododepago(req, resp);
            }
        } else {
            listarMetododepago(req, resp);
        }
    }

    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String accion = req.getParameter("accion");
        if (accion != null) {
            switch (accion) {
                case "crear":
                    this.crear(req, resp);
                    break;
                case "modificar":
                    this.modificar(req, resp);
                    break;
                default:
                    this.listarMetododepago(req, resp);
            }
        } else {
            this.listarMetododepago(req, resp);
        }
    }

    private void listarMetododepago(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<MetodosDePagoDTO> metodosdepago = metDAO.consultar();
        req.setAttribute("metodosdepago", metodosdepago);
        req.getRequestDispatcher("metododepago/verMetodosdepago.jsp").forward(req, resp);
    }

    private MetodosDePagoDTO consultarCli(int debito) {
        for (MetodosDePagoDTO m : metDAO.consultar()) {
            if (m.getDebito() == debito) {
                return m;
            }
        }
        return null;
    }

    private void listarMetodosDePagoVista(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        MetodosDePagoDAO metDAO = new MetodosDePagoDAO();
        List<MetodosDePagoDTO> metodosdepago = metDAO.consultar();
        req.setAttribute("metodosdepago", metodosdepago);
        req.getRequestDispatcher("metododepago/verMetodosdepago.jsp").forward(req, resp);
    }

    private void crear(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

        int debito = Integer.parseInt(req.getParameter("debito"));
        int credito = Integer.parseInt(req.getParameter("cedula"));
        int contraEntrega = Integer.parseInt(req.getParameter("cedula"));

        int registros = new MetodosDePagoDAO().insertar(new MetodosDePagoDTO(debito, credito, contraEntrega));

        this.listarMetododepago(req, resp);
    }

    private void borrar(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

        int debito = Integer.parseInt(req.getParameter("debito"));

        int registro = new MetodosDePagoDAO().borrar(debito);
        this.listarMetododepago(req, resp);
    }

    private void editar(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

        int debito = Integer.parseInt(req.getParameter("debito"));

        MetodosDePagoDTO metododepago = this.consultarCli(debito);
        req.setAttribute("metodosdepago", metododepago);
        req.getRequestDispatcher("metododepago/editarMetododepago.jsp").forward(req, resp);
    }

    public void modificar(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

        int debito = Integer.parseInt(req.getParameter("debito"));
        int credito = Integer.parseInt(req.getParameter("cedula"));
        int contraEntrega = Integer.parseInt(req.getParameter("cedula"));

        int registro = new MetodosDePagoDAO().actualizar(new MetodosDePagoDTO(debito, credito, contraEntrega));
        this.listarMetododepago(req, resp);
    }
}

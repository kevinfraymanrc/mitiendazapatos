/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controller;

import Modelo.Dao.DevolucionDAO;
import Modelo.Entity.DevolucionDTO;
import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author LATITUDE
 */
@WebServlet("/devolucion")
public class DevolucionController extends HttpServlet {

    DevolucionDAO devDAO = new DevolucionDAO();

    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String accion = req.getParameter("accion");
        if (accion != null) {
            switch (accion) {
                case "borrar":
                    this.borrar(req, resp);
                    break;
                case "editar":
                    this.editar(req, resp);
                    break;
                default:
                    listarDevolucion(req, resp);
            }
        } else {
            listarDevolucion(req, resp);
        }
    }

    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String accion = req.getParameter("accion");
        if (accion != null) {
            switch (accion) {
                case "crear":
                    this.crear(req, resp);
                    break;
                case "modificar":
                    this.modificar(req, resp);
                    break;
                default:
                    this.listarDevolucion(req, resp);
            }
        } else {
            this.listarDevolucion(req, resp);
        }
    }

    private void listarDevolucion(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

        List<DevolucionDTO> devoluciones = devDAO.consultar();
        req.setAttribute("devoluciones", devoluciones);
        req.getRequestDispatcher("devolucion/verDevoluciones.jsp").forward(req, resp);
    }

    private DevolucionDTO consultarCli(int averiado) {
        for (DevolucionDTO d : devDAO.consultar()) {
            if (d.getAveriado() == averiado) {
                return d;
            }
        }
        return null;
    }

    private void listarDevolucionesVista(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

        DevolucionDAO devDAO = new DevolucionDAO();
        List<DevolucionDTO> devoluciones = devDAO.consultar();
        req.setAttribute("devoluciones", devoluciones);
        req.getRequestDispatcher("devolucion/verDevoluciones.jsp").forward(req, resp);
    }

    private void crear(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

        int averiado = Integer.parseInt(req.getParameter("averiado"));
        int sucio = Integer.parseInt(req.getParameter("sucio"));
        int malEstado = Integer.parseInt(req.getParameter("malEstado"));
        int diasDeCambio = Integer.parseInt(req.getParameter("diasDeCambio"));

        int registros = new DevolucionDAO().insertar(new DevolucionDTO(averiado, sucio, malEstado, diasDeCambio));
        this.listarDevolucion(req, resp);
    }

    private void borrar(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

        int averiado = Integer.parseInt(req.getParameter("averiado"));

        int registro = new DevolucionDAO().borrar(averiado);
        this.listarDevolucion(req, resp);
    }

    private void editar(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

        int averiado = Integer.parseInt(req.getParameter("averiado"));

        DevolucionDTO devolucion = this.consultarCli(averiado);
        req.setAttribute("devoluciones", devolucion);
        req.getRequestDispatcher("devolucion/editarDevolucion.jsp").forward(req, resp);
    }

    public void modificar(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

        int averiado = Integer.parseInt(req.getParameter("averiado"));
        int sucio = Integer.parseInt(req.getParameter("sucio"));
        int malEstado = Integer.parseInt(req.getParameter("malEstado"));
        int diasDeCambio = Integer.parseInt(req.getParameter("diasDeCambio"));

        int registro = new DevolucionDAO().actualizar(new DevolucionDTO(averiado, sucio, malEstado, diasDeCambio));
        this.listarDevolucion(req, resp);
    }

}

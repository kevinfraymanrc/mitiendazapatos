/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controller;

import Modelo.Dao.ZapatoDAO;
import Modelo.Entity.Zapato;
import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author LATITUDE
 */
@WebServlet("/zapato")
public class ZapatoController extends HttpServlet {

    ZapatoDAO za = new ZapatoDAO();

    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String accion = req.getParameter("accion");
        if (accion != null) {
            switch (accion) {
                case "borrar":
                    this.metodos(req, resp, 2);
                    break;
                case "modificar":
                    this.metodos(req, resp, 3);
                    break;
                default:
                    this.listarProducto(req, resp);
            }
        } else {
            this.listarProducto(req, resp);
        }
    }

    private void listarProducto(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<Zapato> zapatos = za.consultar();
        req.setAttribute("zapatos", zapatos);
        req.getRequestDispatcher("zapato/verZapatos.jsp").forward(req, resp);
    }

    public void metodos(HttpServletRequest req, HttpServletResponse resp, int n) throws ServletException, IOException {
        int id;
        int precio;
        String nombre;

        switch (n) {
            case 1: {  //Crear
                id = Integer.valueOf(req.getParameter("id_zapato"));
                precio = Integer.valueOf(req.getParameter("precio"));
                nombre = req.getParameter("nombre");
                int registros = new ZapatoDAO().insertar(new Zapato(id, precio, nombre));
                this.listarProducto(req, resp);
                break;
            }
            case 2: {  //Borrar
                id = Integer.valueOf(req.getParameter("id_zapato"));
                int registros = this.za.borrar(id);
                this.listarProducto(req, resp);

                break;
            }
            case 3: { //Editar
                id = Integer.valueOf(req.getParameter("id_zapato"));
                Zapato zap = this.consultarZ(id);
                //Producto pro = new Producto(123456, 1000, "producto01");
                req.setAttribute("zapato", zap);
                req.getRequestDispatcher("zapato/editarZapato.jsp").forward(req, resp);
                this.listarProducto(req, resp);
                break;
            }
            case 4: { //Modificar
                id = Integer.valueOf(req.getParameter("id_zapato"));
                precio = Integer.valueOf(req.getParameter("precio"));
                nombre = req.getParameter("nombre");
                int registros = new ZapatoDAO().actualizar(new Zapato(id, precio, nombre));
                this.listarProducto(req, resp);
                break;
            }
        }
    }

    private Zapato consultarZ(int id) {
        Zapato zap = null;
        for (Zapato z : za.consultar()) {
            if (z.getId_zapato() == id) {
                zap = new Zapato(z.getId_zapato(), z.getPrecio(), z.getNombre());
            }
        }
        return zap;
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String accion = req.getParameter("accion");
        if (accion != null) {
            switch (accion) {
                case "crear":
                    this.metodos(req, resp, 1);
                    break;
                case "modificar":
                    this.metodos(req, resp, 4);
                    break;
                default:
                    this.listarProducto(req, resp);
            }
        } else {
            this.listarProducto(req, resp);
        }
    }
}
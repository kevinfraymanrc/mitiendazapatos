/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modelo.Entity;

/**
 *
 * @author LATITUDE
 */
public class CarritoDeComprasDTO {

    private int articulosSeleccionados;
    private int valorUnitario;
    private int valorTotal;
    private int realizarCompra;

    public CarritoDeComprasDTO(int articulosSeleccionados, int valorUnitario, int valorTotal, int realizarCompra) {
        this.articulosSeleccionados = articulosSeleccionados;
        this.valorUnitario = valorUnitario;
        this.valorTotal = valorTotal;
        this.realizarCompra = realizarCompra;
    }

    public int getArticulosSeleccionados() {
        return articulosSeleccionados;
    }

    public void setArticulosSeleccionados(int articulosSeleccionados) {
        this.articulosSeleccionados = articulosSeleccionados;
    }

    public int getValorUnitario() {
        return valorUnitario;
    }

    public void setValorUnitario(int valorUnitario) {
        this.valorUnitario = valorUnitario;
    }

    public int getValorTotal() {
        return valorTotal;
    }

    public void setValorTotal(int valorTotal) {
        this.valorTotal = valorTotal;
    }

    public int getRealizarCompra() {
        return realizarCompra;
    }

    public void setRealizarCompra(int realizarCompra) {
        this.realizarCompra = realizarCompra;
    }

    @Override
    public String toString() {
        return "CarritoDeComprasDTO{" + "articulosSeleccionados=" + articulosSeleccionados + ", valorUnitario=" + valorUnitario + ", valorTotal=" + valorTotal + ", realizarCompra=" + realizarCompra + '}';
    }

}

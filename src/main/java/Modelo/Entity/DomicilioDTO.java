/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modelo.Entity;

/**
 *
 * @author LATITUDE
 */
public class DomicilioDTO {

    private int departamento;
    private int ciudad;
    private int direccion;

    public DomicilioDTO(int departamento, int ciudad, int direccion) {
        this.departamento = departamento;
        this.ciudad = ciudad;
        this.direccion = direccion;
    }

    public int getDepartamento() {
        return departamento;
    }

    public void setDepartamento(int departamento) {
        this.departamento = departamento;
    }

    public int getCiudad() {
        return ciudad;
    }

    public void setCiudad(int ciudad) {
        this.ciudad = ciudad;
    }

    public int getDireccion() {
        return direccion;
    }

    public void setDireccion(int direccion) {
        this.direccion = direccion;
    }

    @Override
    public String toString() {
        return "DomicilioDTO{" + "departamento=" + departamento + ", ciudad=" + ciudad + ", direccion=" + direccion + '}';
    }

}

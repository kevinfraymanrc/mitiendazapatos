/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modelo.Entity;

/**
 *
 * @author LATITUDE
 */
public class Zapato {

    int id_zapato;
    int precio;
    String nombre;

    public Zapato() {
    }

    public Zapato(int id_zapato, int precio, String nombre) {
        this.id_zapato = id_zapato;
        this.precio = precio;
        this.nombre = nombre;
    }

    public int getId_zapato() {
        return id_zapato;
    }

    public void setId_zapato(int id_zapato) {
        this.id_zapato = id_zapato;
    }

    public int getPrecio() {
        return precio;
    }

    public void setPrecio(int precio) {
        this.precio = precio;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public String toString() {
        return "Zapato{" + "id_zapato=" + id_zapato + ", precio=" + precio + ", nombre=" + nombre + '}';
    }

}

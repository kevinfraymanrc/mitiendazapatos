/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modelo.Entity;

/**
 *
 * @author LATITUDE
 */
public class MetodosDePagoDTO {

    private int debito;
    private int credito;
    private int contraEntrega;

    public MetodosDePagoDTO(int debito, int credito, int contraEntrega) {
        this.debito = debito;
        this.credito = credito;
        this.contraEntrega = contraEntrega;
    }

    public int getDebito() {
        return debito;
    }

    public void setDebito(int debito) {
        this.debito = debito;
    }

    public int getCredito() {
        return credito;
    }

    public void setCredito(int credito) {
        this.credito = credito;
    }

    public int getContraEntrega() {
        return contraEntrega;
    }

    public void setContraEntrega(int contraEntrega) {
        this.contraEntrega = contraEntrega;
    }

    @Override
    public String toString() {
        return "MetodosDePagoDTO{" + "debito=" + debito + ", credito=" + credito + ", contraEntrega=" + contraEntrega + '}';
    }

}

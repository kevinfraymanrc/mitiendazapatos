/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modelo.Entity;

/**
 *
 * @author LATITUDE
 */
public class DevolucionDTO {

    private int averiado;
    private int sucio;
    private int malEstado;
    private int diasDeCambio;

    public DevolucionDTO(int averiado, int sucio, int malEstado, int diasDeCambio) {
        this.averiado = averiado;
        this.sucio = sucio;
        this.malEstado = malEstado;
        this.diasDeCambio = diasDeCambio;
    }

    public int getAveriado() {
        return averiado;
    }

    public void setAveriado(int averiado) {
        this.averiado = averiado;
    }

    public int getSucio() {
        return sucio;
    }

    public void setSucio(int sucio) {
        this.sucio = sucio;
    }

    public int getMalEstado() {
        return malEstado;
    }

    public void setMalEstado(int malEstado) {
        this.malEstado = malEstado;
    }

    public int getDiasDeCambio() {
        return diasDeCambio;
    }

    public void setDiasDeCambio(int diasDeCambio) {
        this.diasDeCambio = diasDeCambio;
    }

    @Override
    public String toString() {
        return "DevolucionDTO{" + "averiado=" + averiado + ", sucio=" + sucio + ", malEstado=" + malEstado + ", diasDeCambio=" + diasDeCambio + '}';
    }

}

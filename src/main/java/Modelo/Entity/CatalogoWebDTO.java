/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modelo.Entity;

/**
 *
 * @author LATITUDE
 */
public class CatalogoWebDTO {

    private String referencia;
    private String color;
    private int talla;
    private String material;

    public CatalogoWebDTO(String referencia, String color, int talla, String material) {
        this.referencia = referencia;
        this.color = color;
        this.talla = talla;
        this.material = material;
    }

    public String getReferencia() {
        return referencia;
    }

    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getTalla() {
        return talla;
    }

    public void setTalla(int talla) {
        this.talla = talla;
    }

    public String getMaterial() {
        return material;
    }

    public void setMaterial(String material) {
        this.material = material;
    }

    @Override
    public String toString() {
        return "CatalogoWebDTO{" + "referencia=" + referencia + ", color=" + color + ", talla=" + talla + ", material=" + material + '}';
    }

}

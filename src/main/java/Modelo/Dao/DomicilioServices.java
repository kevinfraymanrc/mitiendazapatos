/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package Modelo.Dao;

import Modelo.Entity.DomicilioDTO;
import java.util.List;

/**
 *
 * @author LATITUDE
 */
public interface DomicilioServices {

    public int insertar(DomicilioDTO domicilio);

    public List<DomicilioDTO> consultar();

    public int borrar(int direccion_domicilio);

    public int actualizar(DomicilioDTO domicilio);
}

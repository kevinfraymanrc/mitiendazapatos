/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modelo.Dao;

import Modelo.Red.ConexionBD;
import Modelo.Entity.CarritoDeComprasDTO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author LATITUDE
 */
public class CarritoDeComprasDAO implements CarritoDeComprasServices {

    public static final String SQL_CONSULTA = "SELECT * FROM CarritoDeCompras";
    public static final String SQL_INSERTAR = "INSERT INTO CarritoDeCompras(articulosSeleccionados, valorUnitario, valorTotal, realizarCompra) VALUES (?,?,?,?)";
    public static final String SQL_DELETE = "DELETE FROM CarritoDeCompras WHERE articulosSeleccionados = ?";
    public static final String SQL_UPDATE = "UPDATE CarritoDeCompras SET valorUnitario = ?, valorTotal = ?, realizarCompra = ? WHERE articulosSeleccionados = ?";

    @Override
    public int insertar(CarritoDeComprasDTO carritoDeCompras) {
        Connection con = null;
        PreparedStatement ps = null;
        int registros = 0;
        try {
            con = ConexionBD.getConnection();
            ps = con.prepareStatement(SQL_INSERTAR);
            ps.setInt(1, carritoDeCompras.getArticulosSeleccionados());
            ps.setInt(2, carritoDeCompras.getValorUnitario());
            ps.setInt(3, carritoDeCompras.getValorTotal());
            ps.setInt(4, carritoDeCompras.getRealizarCompra());
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);

        } finally {
            try {
                ConexionBD.close(ps);
                ConexionBD.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return registros;
    }

    @Override
    public List<CarritoDeComprasDTO> consultar() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet res = null;
        List<CarritoDeComprasDTO> carritosDeCompras = new ArrayList();
        try {
            con = ConexionBD.getConnection();
            ps = con.prepareStatement(SQL_CONSULTA);
            res = ps.executeQuery();
            while (res.next()) {
                int articulosSeleccionados = res.getInt("articulosSeleccionados");
                int valorUnitario = res.getInt("valorUnitario");
                int valorTotal = res.getInt("valorTotal");
                int realizarCompra = res.getInt("realizarCompra");

                CarritoDeComprasDTO carritoDeCompras = new CarritoDeComprasDTO(articulosSeleccionados, valorUnitario, valorTotal, realizarCompra);
                carritosDeCompras.add(carritoDeCompras);
            }
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);

        } finally {
            try {
                ConexionBD.close(res);
                ConexionBD.close(ps);
                ConexionBD.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return carritosDeCompras;
    }

    @Override
    public int borrar(int articulosSeleccionados_carritoDeCompras) {
        Connection con = null;
        PreparedStatement ps = null;
        int registros = 0;
        try {
            con = ConexionBD.getConnection();
            ps = con.prepareStatement(SQL_DELETE);
            ps.setInt(1, articulosSeleccionados_carritoDeCompras);
            registros = ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                ConexionBD.close(ps);
                ConexionBD.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return registros;
    }

    @Override
    public int actualizar(CarritoDeComprasDTO carritoDeCompras) {
        Connection con = null;
        PreparedStatement ps = null;
        int registros = 0;
        try {
            con = ConexionBD.getConnection();
            ps = con.prepareStatement(SQL_UPDATE);

            ps.setInt(1, carritoDeCompras.getValorUnitario());
            ps.setInt(2, carritoDeCompras.getValorTotal());
            ps.setInt(3, carritoDeCompras.getRealizarCompra());
            ps.setInt(4, carritoDeCompras.getArticulosSeleccionados());

            registros = ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                ConexionBD.close(ps);
                ConexionBD.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return registros;
    }
}

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package Modelo.Dao;

import Modelo.Entity.CarritoDeComprasDTO;
import java.util.List;

/**
 *
 * @author LATITUDE
 */
public interface CarritoDeComprasServices {

    public int insertar(CarritoDeComprasDTO carritoDeCompras);

    public List<CarritoDeComprasDTO> consultar();

    public int borrar(int articulosSeleccionados_carritoDeCompras);

    public int actualizar(CarritoDeComprasDTO carritoDeCompras);
}

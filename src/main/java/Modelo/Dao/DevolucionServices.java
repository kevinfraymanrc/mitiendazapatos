/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package Modelo.Dao;

import Modelo.Entity.DevolucionDTO;
import java.util.List;

/**
 *
 * @author LATITUDE
 */
public interface DevolucionServices {

    public int insertar(DevolucionDTO devolucion);

    public List<DevolucionDTO> consultar();

    public int borrar(int averiado_devolucion);

    public int actualizar(DevolucionDTO devolucion);
}

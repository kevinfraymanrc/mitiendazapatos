/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modelo.Dao;

import Modelo.Entity.ClienteDTO;
import Modelo.Red.ConexionBD;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author LATITUDE
 */
public class ClienteDAO implements ClienteServices {

    public static final String SQL_CONSULTA = "SELECT * FROM Cliente";
    public static final String SQL_INSERTAR = "INSERT INTO Cliente(cedula, nombre, apellido, telefono, login, contraseña) VALUES (?,?,?,?,?,?)";
    public static final String SQL_DELETE = "DELETE FROM Cliente WHERE cedula = ?";
    public static final String SQL_UPDATE = "UPDATE Cliente SET nombre = ?, apellido = ?, telefono = ?, login = ?, contraseña = ? WHERE cedula = ?";

    @Override
    public int insertar(ClienteDTO cliente) {
        Connection con = null;
        PreparedStatement ps = null;
        int registros = 0;
        try {
            con = ConexionBD.getConnection();
            ps = con.prepareStatement(SQL_INSERTAR);
            ps.setInt(1, cliente.getCedula());
            ps.setString(2, cliente.getNombre());
            ps.setString(3, cliente.getApellido());
            ps.setInt(4, cliente.getTelefono());
            ps.setString(5, cliente.getLogin());
            ps.setString(6, cliente.getContrasena());
            registros = ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);

        } finally {
            try {
                ConexionBD.close(ps);
                ConexionBD.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return registros;
    }

    @Override
    public List<ClienteDTO> consultar() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet res = null;
        List<ClienteDTO> clientes = new ArrayList();
        try {
            con = ConexionBD.getConnection();
            ps = con.prepareStatement(SQL_CONSULTA);
            res = ps.executeQuery();
            while (res.next()) {
                int cedula = res.getInt("cedula");
                String nombre = res.getString("nombre");
                String apellido = res.getString("apellido");
                int telefono = res.getInt("telefono");
                String login = res.getString("login");
                String contraseña = res.getString("contraseña");

                ClienteDTO cliente = new ClienteDTO(cedula, nombre, apellido, telefono, login, contraseña);
                clientes.add(cliente);
            }
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);

        } finally {
            try {
                ConexionBD.close(res);
                ConexionBD.close(ps);
                ConexionBD.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return clientes;
    }

    @Override
    public int borrar(int cedula_cliente) {
        Connection con = null;
        PreparedStatement ps = null;
        int registros = 0;
        try {
            con = ConexionBD.getConnection();
            ps = con.prepareStatement(SQL_DELETE);
            ps.setInt(1, cedula_cliente);
            registros = ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                ConexionBD.close(ps);
                ConexionBD.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return registros;
    }

    @Override
    public int actualizar(ClienteDTO cliente) {
        Connection con = null;
        PreparedStatement ps = null;
        int registros = 0;
        try {
            con = ConexionBD.getConnection();
            ps = con.prepareStatement(SQL_UPDATE);

            ps.setString(1, cliente.getNombre());
            ps.setString(2, cliente.getApellido());
            ps.setInt(3, cliente.getTelefono());
            ps.setString(4, cliente.getLogin());
            ps.setString(5, cliente.getContrasena());
            ps.setInt(6, cliente.getCedula());

            registros = ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                ConexionBD.close(ps);
                ConexionBD.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return registros;
    }

}

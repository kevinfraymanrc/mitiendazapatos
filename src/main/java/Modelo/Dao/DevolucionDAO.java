/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modelo.Dao;

import Modelo.Entity.DevolucionDTO;
import Modelo.Red.ConexionBD;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author LATITUDE
 */
public class DevolucionDAO implements DevolucionServices {

    public static final String SQL_CONSULTA = "SELECT * FROM Devolucion";
    public static final String SQL_INSERTAR = "INSERT INTO Devolucion(averiado, sucio, malEstado, diasDeCambio) VALUES (?,?,?,?)";
    public static final String SQL_DELETE = "DELETE FROM Devolucion WHERE averiado = ?";
    public static final String SQL_UPDATE = "UPDATE Devolucion SET sucio = ?, malEstado = ?, diasDeCambio = ? WHERE averiado = ?";

    @Override
    public int insertar(DevolucionDTO devolucion) {
        Connection con = null;
        PreparedStatement ps = null;
        int registros = 0;
        try {
            con = ConexionBD.getConnection();
            ps = con.prepareStatement(SQL_INSERTAR);
            ps.setInt(1, devolucion.getAveriado());
            ps.setInt(2, devolucion.getSucio());
            ps.setInt(3, devolucion.getMalEstado());
            ps.setInt(4, devolucion.getDiasDeCambio());

            registros = ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);

        } finally {
            try {
                ConexionBD.close(ps);
                ConexionBD.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return registros;
    }

    @Override
    public List<DevolucionDTO> consultar() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet res = null;
        List<DevolucionDTO> devoluciones = new ArrayList();
        try {
            con = ConexionBD.getConnection();
            ps = con.prepareStatement(SQL_CONSULTA);
            res = ps.executeQuery();
            while (res.next()) {
                int averiado = res.getInt("averiado");
                int sucio = res.getInt("sucio");
                int malEstado = res.getInt("malEstado");
                int diasDeCambio = res.getInt("diasDeCambio");

                DevolucionDTO devolucion = new DevolucionDTO(averiado, sucio, malEstado, diasDeCambio);
                devoluciones.add(devolucion);
            }
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);

        } finally {
            try {
                ConexionBD.close(res);
                ConexionBD.close(ps);
                ConexionBD.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return devoluciones;
    }

    @Override
    public int borrar(int averiado_devolucion) {
        Connection con = null;
        PreparedStatement ps = null;
        int registros = 0;
        try {
            con = ConexionBD.getConnection();
            ps = con.prepareStatement(SQL_DELETE);
            ps.setInt(1, averiado_devolucion);
            registros = ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                ConexionBD.close(ps);
                ConexionBD.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return registros;
    }

    @Override
    public int actualizar(DevolucionDTO devolucion) {
        Connection con = null;
        PreparedStatement ps = null;
        int registros = 0;
        try {
            con = ConexionBD.getConnection();
            ps = con.prepareStatement(SQL_UPDATE);

            ps.setInt(2, devolucion.getSucio());
            ps.setInt(2, devolucion.getMalEstado());
            ps.setInt(3, devolucion.getDiasDeCambio());
            ps.setInt(4, devolucion.getAveriado());

            registros = ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                ConexionBD.close(ps);
                ConexionBD.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return registros;
    }

}

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package Modelo.Dao;

import Modelo.Entity.MetodosDePagoDTO;
import java.util.List;

/**
 *
 * @author LATITUDE
 */
public interface MetodosDePagoServices {

    public int insertar(MetodosDePagoDTO metodosDePago);

    public List<MetodosDePagoDTO> consultar();

    public int borrar(int debito_metodosDePago);

    public int actualizar(MetodosDePagoDTO metodosDePago);
}

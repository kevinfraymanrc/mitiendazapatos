/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modelo.Dao;

import Modelo.Entity.DomicilioDTO;
import Modelo.Red.ConexionBD;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author LATITUDE
 */
public class DomicilioDAO implements DomicilioServices {

    public static final String SQL_CONSULTA = "SELECT * FROM Domicilio";
    public static final String SQL_INSERTAR = "INSERT INTO Domicilio(departamento, ciudad, direccion) VALUES (?,?,?)";
    public static final String SQL_DELETE = "DELETE FROM Domicilio WHERE direccion = ?";
    public static final String SQL_UPDATE = "UPDATE Domicilio SET departamento = ?, ciudad = ? WHERE direccion = ?";

    @Override
    public int insertar(DomicilioDTO domicilio) {
        Connection con = null;
        PreparedStatement ps = null;
        int registros = 0;
        try {
            con = ConexionBD.getConnection();
            ps = con.prepareStatement(SQL_INSERTAR);
            ps.setInt(1, domicilio.getDepartamento());
            ps.setInt(2, domicilio.getCiudad());
            ps.setInt(3, domicilio.getDireccion());;
            registros = ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                ConexionBD.close(ps);
                ConexionBD.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return registros;
    }

    @Override
    public List<DomicilioDTO> consultar() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet res = null;
        List<DomicilioDTO> domicilios = new ArrayList();
        try {
            con = ConexionBD.getConnection();
            ps = con.prepareStatement(SQL_CONSULTA);
            res = ps.executeQuery();
            while (res.next()) {
                int departamento = res.getInt("departamento");
                int ciudad = res.getInt("ciudad");
                int direccion = res.getInt("direccion");
                DomicilioDTO domicilio = new DomicilioDTO(departamento, ciudad, direccion);
                domicilios.add(domicilio);
            }
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                ConexionBD.close(res);
                ConexionBD.close(ps);
                ConexionBD.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return domicilios;
    }
    
    @Override
    public int borrar(int direccion_domicilio) {
        Connection con = null;
        PreparedStatement ps = null;
        int registros = 0;
        try {
            con = ConexionBD.getConnection();
            ps = con.prepareStatement(SQL_DELETE);
            ps.setInt(1, direccion_domicilio);
            registros = ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                ConexionBD.close(ps);
                ConexionBD.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return registros;
    }

    @Override
    public int actualizar(DomicilioDTO domicilio) {
        Connection con = null;
        PreparedStatement ps = null;
        int registros = 0;
        try {
            con = ConexionBD.getConnection();
            ps = con.prepareStatement(SQL_UPDATE);

            ps.setInt(1, domicilio.getCiudad());
            ps.setInt(2, domicilio.getDireccion());
            ps.setInt(3, domicilio.getDepartamento());

            registros = ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                ConexionBD.close(ps);
                ConexionBD.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return registros;
    }

}

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package Modelo.Dao;

import Modelo.Entity.CatalogoWebDTO;
import java.util.List;

/**
 *
 * @author LATITUDE
 */
public interface CatalogoWebServices {

    public int insertar(CatalogoWebDTO catalogoWeb);

    public List<CatalogoWebDTO> consultar();

    public int borrar(String referencia_catalogoWeb);

    public int actualizar(CatalogoWebDTO catalogoWeb);
}

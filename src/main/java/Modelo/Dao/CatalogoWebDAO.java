/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modelo.Dao;

import Modelo.Entity.CatalogoWebDTO;
import Modelo.Red.ConexionBD;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author LATITUDE
 */
public class CatalogoWebDAO implements CatalogoWebServices {

    public static final String SQL_CONSULTA = "SELECT * FROM CatalogoWeb";
    public static final String SQL_INSERTAR = "INSERT INTO CatalogoWeb(referencia, color, talla, material) VALUES (?,?,?,?)";
    public static final String SQL_DELETE = "DELETE FROM CatalogoWeb WHERE referencia = ?";
    public static final String SQL_UPDATE = "UPDATE CatalogoWeb SET color = ?, talla = ?, material = ? WHERE referencia = ?";

    @Override
    public int insertar(CatalogoWebDTO catalogoWeb) {
        Connection con = null;
        PreparedStatement ps = null;
        int registros = 0;
        try {
            con = ConexionBD.getConnection();
            ps = con.prepareStatement(SQL_INSERTAR);

            ps.setString(1, catalogoWeb.getReferencia());
            ps.setString(2, catalogoWeb.getColor());
            ps.setInt(3, catalogoWeb.getTalla());
            ps.setString(4, catalogoWeb.getMaterial());

            registros = ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);

        } finally {
            try {
                ConexionBD.close(ps);
                ConexionBD.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return registros;
    }

    @Override
    public List<CatalogoWebDTO> consultar() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet res = null;
        List<CatalogoWebDTO> catalogosWeb = new ArrayList();
        try {
            con = ConexionBD.getConnection();
            ps = con.prepareStatement(SQL_CONSULTA);
            res = ps.executeQuery();
            while (res.next()) {

                String referencia = res.getString("referencia");
                String color = res.getString("color");
                int talla = res.getInt("talla");
                String material = res.getString("material");

                CatalogoWebDTO catalogoWeb = new CatalogoWebDTO(referencia, color, talla, material);
                catalogosWeb.add(catalogoWeb);
            }
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);

        } finally {
            try {
                ConexionBD.close(res);
                ConexionBD.close(ps);
                ConexionBD.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return catalogosWeb;
    }

    @Override
    public int borrar(String referencia_catalogoWeb) {
        Connection con = null;
        PreparedStatement ps = null;
        int registros = 0;
        try {
            con = ConexionBD.getConnection();
            ps = con.prepareStatement(SQL_DELETE);
            ps.setString(1, referencia_catalogoWeb);
            registros = ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                ConexionBD.close(ps);
                ConexionBD.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return registros;
    }

    @Override
    public int actualizar(CatalogoWebDTO catalogoWeb) {
        Connection con = null;
        PreparedStatement ps = null;
        int registros = 0;
        try {
            con = ConexionBD.getConnection();
            ps = con.prepareStatement(SQL_UPDATE);

            ps.setString(1, catalogoWeb.getColor());
            ps.setInt(2, catalogoWeb.getTalla());
            ps.setString(2, catalogoWeb.getMaterial());
            ps.setString(4, catalogoWeb.getReferencia());

            registros = ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                ConexionBD.close(ps);
                ConexionBD.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return registros;
    }

}

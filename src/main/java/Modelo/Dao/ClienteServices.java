/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package Modelo.Dao;

import Modelo.Entity.ClienteDTO;
import java.util.List;

/**
 *
 * @author LATITUDE
 */
public interface ClienteServices {

    public int insertar(ClienteDTO cliente);

    public List<ClienteDTO> consultar();

    public int borrar(int cedula_cliente);

    public int actualizar(ClienteDTO cliente);
}

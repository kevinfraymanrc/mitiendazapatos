/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modelo.Dao;

import Modelo.Entity.MetodosDePagoDTO;
import Modelo.Red.ConexionBD;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author LATITUDE
 */
public class MetodosDePagoDAO implements MetodosDePagoServices {

    public static final String SQL_CONSULTA = "SELECT * FROM MetodosDePago";
    public static final String SQL_INSERTAR = "INSERT INTO MetodosDePago(debito, credito, contraEntrega) VALUES (?,?,?)";
    public static final String SQL_DELETE = "DELETE FROM MetodosDePago WHERE debito = ?";
    public static final String SQL_UPDATE = "UPDATE MetodosDePago SET credito = ?, contraEntrega = ? WHERE debito = ?";

    @Override
    public int insertar(MetodosDePagoDTO metodosDePago) {
        Connection con = null;
        PreparedStatement ps = null;
        int registros = 0;
        try {
            con = ConexionBD.getConnection();
            ps = con.prepareStatement(SQL_INSERTAR);
            ps.setInt(1, metodosDePago.getDebito());
            ps.setInt(2, metodosDePago.getCredito());
            ps.setInt(3, metodosDePago.getContraEntrega());
            registros = ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);

        } finally {
            try {
                ConexionBD.close(ps);
                ConexionBD.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return registros;
    }

    @Override
    public List<MetodosDePagoDTO> consultar() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet res = null;
        List<MetodosDePagoDTO> metodosDePagos = new ArrayList();
        try {
            con = ConexionBD.getConnection();
            ps = con.prepareStatement(SQL_CONSULTA);
            res = ps.executeQuery();
            while (res.next()) {
                int debito = res.getInt("debito");
                int credito = res.getInt("credito");
                int contraEntrega = res.getInt("contraEntrega");

                MetodosDePagoDTO metodosDePago = new MetodosDePagoDTO(debito, credito, contraEntrega);
                metodosDePagos.add(metodosDePago);
            }
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                ConexionBD.close(res);
                ConexionBD.close(ps);
                ConexionBD.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return metodosDePagos;
    }

    @Override
    public int borrar(int debito_metodosDePago) {
        Connection con = null;
        PreparedStatement ps = null;
        int registros = 0;
        try {
            con = ConexionBD.getConnection();
            ps = con.prepareStatement(SQL_DELETE);
            ps.setInt(1, debito_metodosDePago);
            registros = ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                ConexionBD.close(ps);
                ConexionBD.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return registros;
    }

    @Override
    public int actualizar(MetodosDePagoDTO metodosDePago) {
        Connection con = null;
        PreparedStatement ps = null;
        int registros = 0;
        try {
            con = ConexionBD.getConnection();
            ps = con.prepareStatement(SQL_UPDATE);

            ps.setInt(1, metodosDePago.getCredito());
            ps.setInt(2, metodosDePago.getContraEntrega());
            ps.setInt(3, metodosDePago.getDebito());

            registros = ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                ConexionBD.close(ps);
                ConexionBD.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return registros;
    }

}

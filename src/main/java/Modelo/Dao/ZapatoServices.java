/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package Modelo.Dao;

import Modelo.Entity.Zapato;
import java.util.List;

/**
 *
 * @author LATITUDE
 */
public interface ZapatoServices {
    
    public int insertar(Zapato metodosDePago);

    public List<Zapato> consultar();

    public int borrar(int id_Zapato);

    public int actualizar(Zapato zapato);
}

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    </head>
    <body>
        <form method="post" action="${pageContext.request.contextPath}/domicilio?accion=modificar&direccion=${domicilios.direccion}">
            <label for="validationServer01" class="form-label">Departamento:</label>
            <input type="text" class="form-control is-valid" value="${domicilios.departamento}" id="departamento" name="departamento"  required>

            <label for="validationServer01" class="form-label">Ciudad:</label>
            <input type="number" class="form-control is-valid" value="${domicilios.ciudad}" id="ciudad" name="ciudad"  required>

            <label for="validationServer01" class="form-label">Dirección:</label>
            <input type="number" class="form-control is-valid" value="${domicilios.direccion}" id="direccion" name="direccion"  required>

            <button class="btn btn-primary" type="submit">Guardar</button>
        </form>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

    </body>
</html>

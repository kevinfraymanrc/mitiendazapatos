<%@include file="../admin/cabecera.jsp" %>

<a class="btn btn-primary btn-lg" role="button" aria-disabled="true" href="${pageContext.request.contextPath}/zapato/agregarZapato.jsp">Agregar zapato</a>
<table class="table table-dark table-sm">
    <thead>
        <tr>  
            <th scope="col">ID</th>
            <th scope="col">Precio</th>
            <th scope="col">Nombre</th>
            <th scope="col">Acciones</th>
        </tr>
    </thead>
    <tbody>
        <c:forEach var="zap" items="${zapatos}">
            <tr>
                <td>${zap.id_zapato}</td>
                <td>${zap.precio}</td>
                <td>${zap.nombre}</td>
                <td>
                    <a class="btn btn-primary btn-lg" role="button" aria-disabled="true" href="${pageContext.request.contextPath}/zapato?id_zapato=${zap.id_zapato}&accion=borrar">Eliminar</a>
                    <a class="btn btn-primary btn-lg" role="button" aria-disabled="true" href="${pageContext.request.contextPath}/zapato?id_zapatoo=${zap.id_zapato}&accion=modificar">Actualizar</a>
                </td>
            </tr>
        </c:forEach>
    </tbody>
</table>
<%@include file="../admin/pie.jsp" %>
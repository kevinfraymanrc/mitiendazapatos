<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Agregar Zapato</title>
        <!-- CSS only -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    </head>
    <body>
        <form method="post" action="${pageContext.request.contextPath}/zapato?accion=crear">
            <label for="validationCustom01" class="form-label">Digite ID:</label>
            <input type="number" class="form-control is-valid" id="id_zapato" name="id_zapato" required>
            
            
            <label for="validationCustom01" class="form-label">Digite el Precio:</label>
            <input type="number" class="form-control is-valid" id="precio" name="precio" required>
          
            
            <label for="validationCustom01" class="form-label">Digite el Nombre:</label>
            <input type="text" class="form-control is-valid" id="nombre" name="nombre" required>
            <button class="btn btn-primary" type="submit">Guardar</button>
        </form>
         <!-- JavaScript Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    </body>
</html>
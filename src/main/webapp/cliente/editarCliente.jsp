<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    </head>
    <body>
        <form method="post" action="${pageContext.request.contextPath}/cliente?accion=modificar&cedula=${clientes.cedula}">
            <label for="validationCustom01" class="form-label">Digite el Documento:</label>
            <input type="number" class="form-control is-valid" value="${clientes.cedula}" disabled="disable" id="cedula" name="cedula"  required>
                        
            <label for="validationCustom01" class="form-label">Digite el Nombre:</label>
            <input type="text" class="form-control is-valid" value="${clientes.nombre}" id="nombre" name="nombre"  required>
           
            <label for="validationCustom01" class="form-label">Digite el Apellido:</label>
            <input type="text" class="form-control is-valid" value="${clientes.apellido}" id="apellido" name="apellido"  required>
            
            <label for="validationCustom01" class="form-label">Digite el Telefono:</label>
            <input type="number" class="form-control is-valid" value="${clientes.telefono}" id="telefono" name="telefono"  required>
            
            <label for="validationCustom01" class="form-label">Digite el Login:</label>
            <input type="text" class="form-control is-valid" value="${clientes.login}" id="login" name="login"  required>
                     
            <label for="validationCustom01" class="form-label">Digite el contraseña:</label>
            <input type="text" class="form-control is-valid" value="${clientes.contrasena}" id="contrasena" name="contrasena"  required>
            
            <button class="btn btn-primary" type="submit">Guardar</button>
        </form>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

    </body>
</html>

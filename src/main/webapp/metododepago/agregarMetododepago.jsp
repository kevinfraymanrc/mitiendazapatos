<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Agregar Metodo de pago</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    </head>
    <body>
        <form method="post" action="${pageContext.request.contextPath}/metododepago?accion=crear">
            <label for="validationServer01" class="form-label">Debito:</label>
            <input type="text" class="form-control is-valid" id="debito" name="debito"  required>

            <label for="validationServer01" class="form-label">Credito:</label>
            <input type="text" class="form-control is-valid" id="credito" name="credito"  required>

            <label for="validationServer01" class="form-label">Contra Entrega:</label>
            <input type="text" class="form-control is-valid" id="contraEntrega" name="contraEntrega"  required>

            <button class="btn btn-primary" type="submit">Guardar</button>
        </form>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

    </body>
</html>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <!-- CSS only -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    </head>
    <body>
        <a class="btn btn-primary btn-lg" role="button" aria-disabled="true" href="${pageContext.request.contextPath}/devolucion/agregarDevolucion.jsp">Agregar devolucion :)</a>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th scope="col">Averiado</th>
                    <th scope="col">Sucio</th>
                    <th scope="col">Mal Estado</th>
                    <th scope="col">Dias de Cambio</th>
                    <th scope="col">Acciones</th>
                </tr>
            </thead>
            <tbody>           
                <c:forEach var="dev" items="${devoluciones}">
                    <tr>
                        <td>${dev.averiado}</td>
                        <td>${dev.sucio}</td>
                        <td>${dev.malEstado}</td>
                        <td>${dev.diasDeCambio}</td>
                        <td>
                            <a class="btn btn-primary btn-lg" role="button" aria-disabled="true" href="${pageContext.request.contextPath}/devolucion?averiado=${dev.averiado}&accion=borrar">Eliminar</a>
                            <a class="btn btn-primary btn-lg" role="button" aria-disabled="true" href="${pageContext.request.contextPath}/devolucion?averiado=${dev.averiado}&accion=editar">Editar</a>
                        </td>
                    </tr>                  
                </c:forEach>
            </tbody>
        </table>
        <!-- JavaScript Bundle with Popper -->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    </body>
</html>

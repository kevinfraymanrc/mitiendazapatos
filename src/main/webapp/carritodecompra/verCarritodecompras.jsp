<%-- 
    Document   : vercarritosdecompras
    Created on : 17 dic. 2021, 21:55:18
    Author     : Kevin Frayman Romero Cuellar
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <!-- CSS only -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    </head>
    <body>
        <a class="btn btn-primary btn-lg" role="button" aria-disabled="true" href="${pageContext.request.contextPath}/carritodecompra/agregarCarritodecompra.jsp">Agregar carrito de compra :)</a>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th scope="col">Articulos Seleccionados</th>
                    <th scope="col">Valor Unitario</th>
                    <th scope="col">Valor Total</th>
                    <th scope="col">Realizar Compra</th>
                    <th scope="col">Acciones</th>
                </tr>
            </thead>
            <tbody>           
                <c:forEach var="car" items="${carritodecompras}">
                    <tr>
                        <td>${car.articulosSeleccionados}</td>
                        <td>${car.valorUnitario}</td>
                        <td>${car.valorTotal}</td>
                        <td>${car.realizarCompra}</td>
                        <td>
                            <a class="btn btn-primary btn-lg" role="button" aria-disabled="true" href="${pageContext.request.contextPath}/carritodecompra?articulosSeleccionados=${car.articulosSeleccionados}&accion=borrar">Eliminar</a>
                            <a class="btn btn-primary btn-lg" role="button" aria-disabled="true" href="${pageContext.request.contextPath}/carritodecompra?articulosSeleccionados=${car.articulosSeleccionados}&accion=editar">Editar</a>
                        </td>
                    </tr>                  
                </c:forEach>
            </tbody>
        </table>
        <!-- JavaScript Bundle with Popper -->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    </body>
</html>

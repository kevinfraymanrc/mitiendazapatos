<%-- 
    Document   : editarcarritodecompra
    Created on : 18 nov. 2021, 08:55:18
    Author     : Kevin Frayman Romero Cuellar
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    </head>
    <body>
        <form method="post" action="${pageContext.request.contextPath}/carritodecompra?accion=modificar&articulosSeleccionados=${carritodecompras.articulosSeleccionados}">
            <label for="validationServer01" class="form-label">Articulos Seleccionados:</label>
            <input type="text" class="form-control is-valid" value="${carritodecompras.articulosSeleccionados} id="articulosSeleccionados" name="articulosSeleccionados"  required>

            <label for="validationServer01" class="form-label">Valor Unitario:</label>
            <input type="number" class="form-control is-valid" value="${carritodecompras.valorUnitario} id="valorUnitario" name="valorUnitario"  required>

            <label for="validationServer01" class="form-label">Valor Total:</label>
            <input type="number" class="form-control is-valid" value="${carritodecompras.valorTotal} id="valorTotal" name="valorTotal"  required>

            <label for="validationServer01" class="form-label">Realizar Compra:</label>
            <input type="text" class="form-control is-valid" value="${carritodecompras.realizarCompra} id="realizarCompra" name="realizarCompra"  required>

            <button class="btn btn-primary" type="submit">Guardar</button>
        </form>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

    </body>
</html>
